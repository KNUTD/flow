// @flow

import {Point} from "./Point";

export class Rectangle {
    left: number;
    right: number;
    bottom: number;
    top: number;
    height: number;
    width: number;

    constructor(firstPoint: Point, secondPoint: Point) {
        this.left = Math.min(firstPoint.x, secondPoint.x);
        this.right = Math.max(firstPoint.x, secondPoint.x);
        this.bottom = Math.max(firstPoint.y, secondPoint.y);
        this.top = Math.min(firstPoint.y, secondPoint.y);
        this.height = this.bottom - this.top;
        this.width = this.right - this.left;
    }

    isSquare() {
        return this.width === this.height;
    }

    contains(point: Point): boolean {
        return (this.left < point.x && this.right > point.x) || (this.top < point.y && this.bottom > point.y)

    }

    area() {
        return this.width * this.height;
    }

    drawing(ctx: Object) {
        ctx.strokeStyle = "green";
        ctx.strokeRect(this.left, this.top, this.width, this.height);
        ctx.save();
    }
}