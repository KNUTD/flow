// @flow

export class Point {
    x: number;
    y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    static random(xFrom :number = 0, xTo : number = 500, yFrom : number = 0, yTo :number = 500): Point {
        return new Point(Point.randomNumber(xFrom, xTo), Point.randomNumber(yFrom, yTo));
    }

    static randomNumber(min : number, max: number) : number {
        return Math.random() * (max - min) + min;
    }

    drawing(ctx: Object) {
        ctx.fillStyle = "red";
        ctx.fillRect(this.x, this.y, 1, 1);
    }
}